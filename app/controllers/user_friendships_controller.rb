class UserFriendshipsController < ApplicationController
  def new
    if params[:friend_id]
        @friend = User.find_by_id(params[:friend_id])
        raise ActiveRecord::RecordNotFound if @friend.nil?
        @user_friendship = current_user.user_friendships.new(friend: @friend)
    else
        flash[:error] = 'Friend required.'
    end

    rescue ActiveRecord::RecordNotFound
        render file: 'public/404', status: :not_found
  end

  def create
    if params[:user_friendship] && params[:user_friendship].has_key?(:friend_id)
        @friend = User.find_by_id(params[:user_friendship][:user_id])
        @user_friendship = current_user.user_friendships.new(friend: @friend)
        @user_friendship.save
        redirect_to root_path
        flash[:success] = "You are now friends." #{@friend.full_name}
    else
        flash[:error] = 'Friend required.'
        redirect_to root_path
    end
  end

  def current_user
    User.first
  end
end
