class User < ActiveRecord::Base
  has_many :user_friendships
  has_many :friends, through: :user_friendships
end
