class CreateUserFriendships < ActiveRecord::Migration
  def change
    create_table :user_friendships do |t|
      t.references :user, index: true
      t.references :friend, index: true

      t.timestamps
    end
  end
end
